class Employee(object):

    def __init__(self, fullname, position="QA", experience=1, hobby=None, salary=0):
        self.fullname = fullname
        self.position = position
        self.experience = experience;
        self.hobby = hobby
        self.salary = salary

    def set_fullname(self, fullname):
        self.fullname = fullname

    def set_position(self, position):
        self.position = position

    def set_hobby(self, hobby):
        self.hobby = hobby

    def get_fullname(self):
        return self.fullname

    def get_name(self):
        if self.fullname is not None:
            return self.fullname.split()[0]

    def get_surname(self):
        if self.fullname is not None:
            return self.fullname.split()[-1]

    def caclulate_income(self, months=0):
        if type(months) != int:
            raise ValueError("Please set month as param")
        return self.salary * months

    def __str__(self):
        return "Name: " + self.fullname + "\nPosition: " \
               + self.position + "\nHas hobby: " + str(self.hobby) + "\n"


class ITEmployee(Employee):

    '''Specific ITEmployee child of Employee'''
    def __init__(self, fullname, position="QA", experience=1, hobby=None, salary=0):
        super(ITEmployee, self).__init__(fullname, position, experience, hobby, salary)
        self.skills = []

    def get_relevant_experience(self):
        if type(self.experience) != int:
            raise ValueError("Expirience expected to be integer")

        if self.experience < 3:
            return "Junior " + self.position
        elif 3 <= self.experience <= 6:
            return "Middle " + self.position
        elif self.experience > 6:
            return "Senior " + self.position

    def add_skills(self, new_skill):
        self.skills.append(new_skill)

if __name__ == "__main__":
    pass