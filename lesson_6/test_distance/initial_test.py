import unittest

from lesson_6.geometry.distanse.point import Point


class TestDistance(unittest.TestCase):

    def setUp(self):
        self.point = Point(0, 0)

    @unittest.skip("demonstrating skipping")
    def test_skip(self):
        pass

    def test_distance_with_coords_input(self):
        self.assertEqual(self.point.calculate_distance_coord(3, 4), 5)

    def test_distance_with_object(self):
        point_1 = Point(3, 4)
        point_2 = Point(0, 0)
        self.assertEqual(point_2.calculate_distance(point_1), 5)

    def test_error_return(self):
        self.assertEqual(self.point.calculate_distance_coord(0, 'test'), "Specify correct point coordinates")

    def test_fail(self):
        point = Point(3, 4)
        with self.assertRaises(ValueError, msg="Please set point as param"):
            point.calculate_distance("test")

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main(verbosity=2)