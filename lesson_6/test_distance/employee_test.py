import unittest

from lesson_6.employee import Employee
from lesson_6.employee import ITEmployee


class Employee_test(unittest.TestCase):

    def setUp(self):
        self.employee = Employee("Vins Libertian Order", "QA Automation", 3.5, "shooting", 2000)

    def tearDown(self):
        pass

    def test_get_name(self):
        self.assertEqual(self.employee.get_name(), "Vins")

    def test_get_surname(self):
        self.assertEqual(self.employee.get_surname(), "Order")

    def test_calculate_income(self):
        self.assertEqual(self.employee.caclulate_income(14), 28000)

    def test_full_position_description(self):
        it_employee = ITEmployee("Edisson Trent", "Web Dev", 4, "shooting", 2000)
        self.assertEqual(it_employee.get_relevant_experience(), "Middle Web Dev")

if __name__ == '__main__':
    unittest.main(verbosity=2)