from math import sqrt

class Point(object):

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def calculate_distance_coord(self, x1, y1):
        if type(x1) == int and type(y1) == int:
            return sqrt((x1 - self.x)**2 + (y1 - self.y)**2)
        else:
            return "Specify correct point coordinates"

    def calculate_distance(self, point):
        if type(point) == Point:
            return sqrt((point.x - self.x)**2 + (point.y - self.y)**2)
        else:
            raise ValueError("Please set point as param")
