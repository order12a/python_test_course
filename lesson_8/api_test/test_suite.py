import unittest
import HtmlTestRunner

from lesson_8.api_test.book_test import BookTest
from lesson_8.api_test.user_test import UserTest

suite = unittest.TestLoader().loadTestsFromTestCase(BookTest)
unittest.TextTestRunner(verbosity=2).run(suite)

