import unittest
import requests
import random
import HtmlTestRunner
import sys
import lesson_8.api_test.books_helper as books_helper


class BookTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base_url = 'http://pulse-rest-testing.herokuapp.com/'
        cls.books_relative_url = 'books/'
        cls.roles_relative_url = 'roles/'

    def setUp(self):
        data = {'title': 'Taf Voyage' + str(random.randint(1, 100)), 'author': 'J. R. Martin'}
        self.book = books_helper.create_book(self.base_url + self.books_relative_url, data)

    def tearDown(self):
        requests.delete(self.base_url + self.books_relative_url + str(self.book['id']))

    @classmethod
    def tearDownClass(cls):
        books_helper.clean_up(cls.base_url + cls.books_relative_url)

    def test_book_creation(self):
        original_book = {'title': 'The Plague Star', 'author': 'J. R. Martin'}
        resp = requests.post(self.base_url + self.books_relative_url, original_book)

        new_book = resp.json()
        books_helper.add_book(new_book)

        for key in original_book:
            self.assertEqual(original_book[key], new_book[key])

    def test_get_books(self):
        resp = requests.get(self.base_url + self.books_relative_url)
        self.assertEqual(resp.status_code, 200)
        books = resp.json()
        for book in books:
            self.assertNotEqual(book['title'], None)

    def test_get_book(self):
        resp = requests.get(self.base_url + self.books_relative_url + str(self.book['id']))
        new_book = resp.json()
        self.assertEqual(new_book['title'], self.book['title'])
        self.assertEqual(new_book['author'], self.book['author'])

    def test_update_book(self):
        resp = requests.put(self.base_url + self.books_relative_url + str(self.book['id']) + '/', data={'id': str(self.book['id']), 'title': 'my name', 'author': 'J Martin'})
        updated_book = resp.json()
        self.assertNotEqual(updated_book['title'], self.book['title'])

    def test_delete_book(self):
        my_book = books_helper.get_book()
        print(my_book)
        print(my_book['id'])
        print(self.base_url + self.books_relative_url + str(my_book['id']))

        resp = requests.delete(self.base_url + self.books_relative_url + str(my_book['id']))
        self.assertEqual(resp.status_code, 204)

    @unittest.skip('Skipped test')
    def test_skipped(self):
        pass

    @unittest.skipIf(sys.platform.startswith('linux'), 'Skipped at linux platform')
    def test_skipp_condition(self):
        pass

if __name__ == '__main__':
    # unittest.main(verbosity=2)
    # test1 = BookTest('test_get_books')
    # result = test1.run()
    # print(result)
    #
    # suite = unittest.TestLoader().loadTestsFromTestCase(BookTest)
    # unittest.TextTestRunner(verbosity=2).run(suite)

    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='test_results'), verbosity=2)
