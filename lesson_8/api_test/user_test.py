import unittest
import requests
import random


class UserTest(unittest.TestCase):

    def setUp(self):
        self.base_url = 'http://pulse-rest-testing.herokuapp.com'
        self.books_relative_url = '/books/'
        self.roles_relative_url = '/roles/'

        resp = requests.post(self.base_url + self.roles_relative_url, data={"name": "Heviland Taf", "type": "eco enginner", "level": 100, "book": 980})
        self.user = resp.json()
        print(self.user)

    def tearDown(self):
        pass

    def test_get_user(self):
        resp = requests.get(self.base_url + self.roles_relative_url + str(self.user['id']))
        get_user = resp.json()
        self.assertEqual(get_user['name'], self.user['name'])

if __name__ == '__main__':
    unittest.main(verbosity=2)