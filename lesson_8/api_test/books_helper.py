import requests

books = []


def add_book(new_book):
    books.append(new_book)


def get_book():
    return books.pop()


def create_book(url, data):
    resp = requests.post(url, data=data)
    return resp.json()


def clean_up(base_url):
    for book in books:
        requests.delete(base_url + str(book['id']))
