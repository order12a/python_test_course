#
# greeting = input('Enter your greeting')
# if str(greeting).find('Hi') > -1:
#     print('Hello man!')
# else:
#     print('WARNING!')

# input_number = input('Enter your number \n')
# if int(input_number) > 0 and int(input_number) <= 100:
#     print('Correct number')
# else:
#     print('Incorrect number')

# var = (9, 'test', 1, 2, 4, 'test')
# print (var.count('test'))
# print(type (var))
#
# my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 22, 31, 24]

# while len(my_list) > 0:
#     print(my_list.pop(-1))
#     print(my_list)
# print(my_list)
#
# for i in range(len(my_list)):
#     if my_list[i]%2 == 1:
#         my_list[i] = my_list[i] + 1
#         print(my_list[i])
#
# print(my_list)

# car = {
#     'speed': 162,
#     'type': 'sedan',
#     'fuel': 'gasoline',
#     1: 'no'
# }
#
# for i in car:
#     print(str(i) + ': ' + str(car[i]))


# def calc_hip(x, y):
#     if x <= 0 or y <= 0 :
#         return
#     return (x * x + y * y)**(1/2)
#
# print(calc_hip(3, 4))
# print(calc_hip(4, 4))
# print(calc_hip(1, 2))


def func(*args):
    my_list = list(args)
    my_list.sort()
    counter = 0
    for i in my_list:
        if(counter > 0 and counter%2 == 0):
            print(i)
        counter = counter + 1
    return args

func(1, 3, 22, 21, 33, 111, 23)