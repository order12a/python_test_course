import requests


url = "http://httpbin.org/get"
headers = {'user-agent': 'my-app/0.0.1'}

# def get(request_body, params={}):
#     response = requests.get(request_body)
#     return response
#
# print(get("https://en.wikipedia.org").status_code)
# print(get("https://en.wikipedia.org/wiki/Newcastle_Interchange_railway_station").text)

response = requests.get(url, headers=headers)
dict = response.json()
print(response.headers)
print(dict['headers']['User-Agent'])
