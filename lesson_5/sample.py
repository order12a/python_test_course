class Employee(object):

    general_atrr = 'ATRR'

    def __init__(self, surname, name="Unit", position="QA", hobby=None, salary=0):
        self.surname = surname
        self.name = name
        self.position = position
        self.hobby = hobby
        self.salary = salary

    def set_name(self, name):
        self.name = name

    def set_surname(self, surname):
        self.surname = surname

    def set_position(self, position):
        self.position = position

    def set_hobby(self, hobby):
        self.hobby = hobby

    def full_name(self):
        return self.name + " " + self.surname

    def caclulate_income(self, months=0):
        if type(months) != int:
            raise ValueError("Please set month as param")
        return self.salary * months

    def __str__(self):
        return "Name: " + self.name + "\nSurname: " + self.surname + "\nPosition: " \
               + self.position + "\nHas hobby: " + str(self.hobby) + "\n"

class ITEmployee(Employee):

    '''Specific Employee'''
    def __init__(self, surname, name="Unit", position="QA", hobby=None, salary=0):
        super(ITEmployee, self).__init__(surname, name, position, hobby, salary)
        self.skills = []

    def add_skills(self, new_skill):
        self.skills.append(new_skill)

if __name__ == "__main__":
    e = Employee("Test")

    sam = Employee('Buttler', 'Sam', 'Dev', 'Wrestling', 1800)

    # sam.set_name("Sam")
    # sam.set_position("Doctor")

    # john = Employee()
    # john.set_position("Taylor")
    # john.set_name("John")
    # print(dir(sam))
    print(sam.name + " " + sam.surname + " works as " + sam.position + " with hobby ")
    print("-------------------------------")
    print(e.name + " " + e.surname + " works as " + e.position)
    print("-------------------------------")
    print(sam)
    print(e)
    print("-------------------------------")
    print(sam.full_name())
    print("-------------------------------")
    print(sam.caclulate_income(22))
    print("-------------------------------")
    bill = ITEmployee('Geits', 'Bill', 'Architect', 'Entrepreneur', 1800)
    bill.add_skills("C++")
    print(bill.skills)
    print(bill)

    # print(Employee.general_atrr)
    # print(sam.general_atrr)